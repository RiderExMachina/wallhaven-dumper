#!/usr/bin/env python3

## Written by RiderExMachina May 2019
###
## Chrome unix cookie decryptor written by n8henrie 
## Latest code for the unix cookie decryptor can be found at 
## https://gist.github.com/n8henrie/8715089#file-pycookiecheat-py
import sys, shutil, os, sqlite3


class cookies:
	def __init__(self, name='', value='', table='', field='', domain=''):
		self.value = value
		self.table = table
		self.field = field
		self.domain = domain

	def firefox(self):
		self.name = "Firefox"
		self.value = "value"
		self.table = "moz_cookies"
		self.field = "basedomain"
		self.domain = "'wallhaven.cc'"
	def chrome(self):
		self.name = "Chrome"
		self.value = "encrypted_value"
		self.table = "cookies"
		self.field = "host_key"
		self.domain = "'alpha.wallhaven.cc'"

platform = sys.platform
userFolder = os.path.expanduser("~")
linuxFirefoxFolder = os.path.join(userFolder, ".mozilla/firefox")
linuxChromeFolder = os.path.join(userFolder, ".config/google-chrome/Default")
darwinFirefoxFolder = os.path.join(userFolder, "Library/Application Support/Firefox")
darwinChromeFolder = os.path.join(userFolder, "Library/Application Support/Google/Chrome")

cookies = cookies()

def chromeDecrypt(encryptedValue):
	## Trim the 'v10'
	encryptedValue = encryptedValue[3:]
	
	#Default salt values
	salt = b'saltysalt'
	iv = b' ' * 16
	length = 16

	if platform == 'linux':
		print("Setting platform to Linux")
		mypass = 'peanuts'
		iterations = 1
	elif platform == 'darwin':
		import keyring
		mypass = keyring.get_password('Chrome Safe Storage', 'Chrome')
		iterations = 1003

	mypass = mypass.encode('utf')
	key = PBKDF2(mypass, salt, length, iterations)
	cipher = AES.new(key, AES.MODE_CBC, IV=iv)
	decrypted = cipher.decrypt(encryptedValue)

	decryptedValue = decrypted[:-decrypted[-1]].decode('utf8')
	return decryptedValue


def extraction(database):
	conn = sqlite3.connect(database)

	cookie = conn.execute("SELECT name, {} FROM {} WHERE {}={}".format(cookies.value, cookies.table, cookies.field, cookies.domain) )

	try:
		for entry in cookie:
			if entry[0].startswith("remember_web_"):
				print("Extracting cookie...")
				if "firefox" in database.lower():
					wallhavenCookie = { entry[0] : entry[1] }
					print("Successfully found cookie {}".format(entry[0]))
				elif "chrome" in database.lower():
					#print("Found {} : {}".format(entry[0], entry[1]))
					encryptedValue = entry[1]

					decryptedValue = chromeDecrypt(encryptedValue)
					wallhavenCookie = { entry[0] : decryptedValue }
					print("Successfully found and decrypted cookie {}".format(entry[0]))
		
		return wallhavenCookie

	except UnboundLocalError: 	
		print("Please log into wallhaven in {} to set the cookie!".format(cookies.name))
	
	conn.close()


def firefoxSetup(browserFolder, infoFile):
	infoDump = open(infoFile, 'r').readlines()

	for line in infoDump:
		if line.startswith("Path="):
			profile = line.split("=")[-1].strip("\n")
			print(f"Found profile {profile}")
	
	database = os.path.join(browserFolder, profile, "cookies.sqlite.bak1")
	## Move database folder to remove the lock (if Firefox is open)
	shutil.copy(database.strip(".bak1"), database)
	return extraction(database)

def chromeSetup(browserFolder):
	database = os.path.join(browserFolder, "Cookies")
	return extraction(database)

def browserCheck():
	if platform == 'linux':
		FirefoxFolder = linuxFirefoxFolder
		ChromeFolder = linuxChromeFolder
	elif platform == 'darwin':
		FirefoxFolder = darwinFirefoxFolder
		ChromeFolder = darwinChromeFolder
	## To be tested
	elif platform == 'nt':
		FirefoxFolder = windowsFirefoxFolder
		ChromeFolder = windowsFirefoxFolder


	if os.path.isdir(FirefoxFolder):
		print("Setting browser as \"Firefox\"")
		cookies.firefox()
		browserFolder = FirefoxFolder 
		infoFile = os.path.join(browserFolder, "profiles.ini")

		return firefoxSetup(browserFolder, infoFile)

	elif os.path.isdir(ChromeFolder):
		print("Setting as browser \"Chrome\"")
		## Only import the necessary PyCrypto packages if Chrome is installed
		from Crypto.Cipher import AES
		from Crypto.Protocol.KDF import PBKDF2
		
		cookies.chrome()
		browserFolder = ChromeFolder

		return chromeSetup(browserFolder)
