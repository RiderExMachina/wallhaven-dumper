# Wallhaven Wallpaper Dumper

## About
This tool will dump any wallpaper from https://wallhaven.cc, so long as it exists. It accepts four arguments at the moment: `--pages`, `--ratio`, `--size`, and `--tag`. If no arguments are passed (i.e. just running `python3 wallhaven-dumper.py`) its default values are **pages=10**, **ratio=16x9**, **size=1920x1080**, with no tag (giving random results), and the tool will download the first 10 pages worth of images.

Please note: this is a commandline tool, meaning you will need to run it from the terminal or command prompt. It *should* work just fine on any operating system with Python3 installed, but I have only tested on Fedora 30. If you are unsure how to run this program, follow the directions under your operating system. Otherwise, skip to [Usage](#usage)

#### Windows
1. Make sure Python 3.5+ is installed on your system and is in your PATH. If it is not, or you are unsure, download and install [the latest version of Python](https://www.python.org/ftp/python/3.7.3/python-3.7.3-amd64.exe). **Make sure you check the box that says "Add Python to PATH"!**
2. Open CMD or Powershell using your preferred method (usually Start Menu or Win+R and then typing "cmd" or "powershell").
3. Ensure Python is installed by typing `python`. If the Python shell does not come up, try reinstalling Python.
4. Perform `pip install requests`
5. Perform `cd %userprofile%\path\to\wallhaven-dumper.py` then skip to [Usage](#usage).

#### macOS
1. Make sure Python 3.5+ is installed on your system and is in your PATH. If it is not, download and install [the latest version of Python](https://www.python.org/ftp/python/3.7.3/python-3.7.3-macosx10.9.pkg).
2. Open your favorite terminal application (usually Applications > Utilities > Terminal).
3. Ensure Python is installed by typing `python`. If the Python shell does not come up, try reinstalling Python.
4. Perform `pip install requests` 
5. Perform `cd path/to/wallhaven-dumper.py` then skip to [Usage](#usage)

#### Linux
1. Make sure Python3 is installed on your system. It usually is by default, but if not, install it through your package manager (`sudo apt install python3`, `sudo dnf install python3`, `sudo pacman -S python3`, etc.)
2. Open a terminal window (usually CTRL+ALT+T or META+T by default)
3. Type `python` and note if you get Python 2 or Python 3 (On Fedora 30 I get `Python 2`) and then press CTRL+D to close the Python emulator
4. Perform `cd path/to/wallhaven-dumper.py` then skip to [Usage](#usage)

## [Usage](#usage)

#### Requirements:
This tool requires the `requests` and `bs4` packages from pip. If you want to download NSFW images you must have a wallhaven account. Log into Wallhaven and then see the browser specific requirements:

##### Firefox:

No special requirements are needed. Run `pip install requests bs4` or `pip3 install requests bs4` in your terminal/command prompt. 

##### Chrome:

At the very least you need `PyCrypto`. If you are on Mac you also need `keyring`. Run `pip install requests bs4 PyCrypto keyring` or `pip3 install requests bs4 PyCrypto keyring` in your terminal/command prompt.

#### Examples:

Get ~200 random wallpapers 1920x1080 or bigger:

`python3 wallhaven-dumper.py`

Get ~60 random wallpapers 1920x1080 or bigger:

`python3 wallhaven-dumper.py --pages 3`

Get ~200 wallpapers 1280x720 or bigger:

`python3 wallhaven-dumper.py --size 1280x720`

Get ~200 wallpapers of 4x3 ratio:

`python3 wallhaven-dumper.py --ratio 4x3`

Get ~200 wallpapers with the anime tag:

`python3 wallhaven-dumper.py --tag anime`

Note:
You also use `-p` for `--pages`, `-s` for `--size`, `-r` for `--ratio`, and `-t` for `--tag`

If no tags are used, the images will save to `Wallhaven-Random` in your User's Pictures folder. If you use one tag (e.g. `-t anime`), it will save to a folder named `Wallhave Tag: Anime`. If you use multiple tags (e.g. `-t anime,girls,headphones`) it will save to a folder named `Wallhaven Tags: Anime Girls Headphones`.