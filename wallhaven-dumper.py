#!/usr/bin/env python3

"""
Created by RiderExMachina May 2019

wallhaven-dumper.py
Description: Download random wallpapers from wallhaven.cc

Version: 0.1.5
"""

import sys, argparse, os
try:
    import requests
except:
    print('Missing "requests" package. Please make sure the python3-pip package is instealled and run "sudo pip3 install requests".')

try:
    from bs4 import BeautifulSoup as bsoup
except:
    print('Missing "bs4" package. Please make sure the python3-pip package is installed and run "sudo pip3 install bs4".')

if os.path.isfile("cookieExtractor.py"):
    import cookieExtractor

parser = argparse.ArgumentParser()

parser.add_argument('-p', '--page', help='The number of pages you want to download. Each pages is about 24 images. Default is 10.', default='10')
parser.add_argument('-r', '--ratio', help='Search for a specific ratio (e.g. 4x3 or 16x9). Default is 16x9.', default='16x9')
parser.add_argument('-s', '--size', help='Search for a specific wallpaper minimum size (e.g. 1280x720). Default is 1920x1080.', default='1920x1080')
parser.add_argument('-t', '--tag', help='Search for wanted tag (e.g. anime). Default is "random".', default='')
parser.add_argument('-x', '--purity', help='How risque the wanted results are. Default is SFW (Level 1).', default='100')

args = parser.parse_args()

## Set home folder for the current user, should work on all major platforms
homeFolder = os.path.expanduser("~")

## Set dictionary for purity levels: this should give a good start.
## Should be able to add some more if people think of more synonyms
purityLevels = { 
				'nsfw' : [
						  'explicit',
						  'nsfw',
						  'xxx',
						  '001',
						  '3'
						  ],
				'sketchy' : [
							 'sketchy',
							 '010',
							 '2'
								 ]
}

#TODO: fix the spaghetti
cookie = None

horny = args.purity
if horny in purityLevels["nsfw"]:
	## NSFW pictures require a login for the site
	cookie = cookieExtractor.browserCheck()
	if cookie != None:
		print("Purity set to NSFW")
		purity = "001"
	else:
		print("Please see the documentation for more info.")
		purity = "100"
elif horny in purityLevels["sketchy"]:
	print("Purity set to Sketchy")
	purity = "010"
else:
	purity = "100"


def finished(downloadpath):
	print("Images were downloaded to {}".format(downloadpath))
	exit(0)

def downloadWallpapers(downloadpath, image):
	## Get the "name" of the image, which is the last item in the URL
	imageName = image.split('/')[-1]
	## It seems that the folder the images are in is just the first two characters of the imagename
	## so we grab the first two characters and place it in the URL.
	## Example URL of the new system: https://w.wallhaven.cc/full/ne/wallhaven-nem13l.jpg
	folderName = imageName[0:2]

	baseURL = "https://w.wallhaven.cc/full/{}/wallhaven-{}".format(folderName, imageName)
	mimeTypes = [".png", ".jpg"]
	## Find out if the image is a png or a jpg and download it
	for mime in mimeTypes:
		image = baseURL + mime
		## If you don't check if the image already exists the program will download a 0 byte file (not kosher) 
		if requests.head(image).status_code == 200:
			imageDownloadPath = os.path.join(downloadpath, imageName + mime)
			if os.path.isfile(imageDownloadPath):
				print("{} already exists!".format(imageName))
			else:
				with open(imageDownloadPath, 'wb') as wallpaper:
					wallpaper.write(requests.get(image).content)
				print("Downloaded {}".format(imageName))

def investigateWallpapers(ratio, size, tag, purity):
	## Check the ~/.config/user-dirs.dirs file for XDG compliance with non-English locales on Linux 
	xdgFile = os.path.join(homeFolder, ".config/user-dirs.dirs")
	if sys.platform == 'linux' and os.path.isfile(xdgFile):
		with open(xdgFile, 'r') as dirsList:
			for line in dirsList:
				if line.startswith("XDG_PICTURES_DIR"):
					picturesFolder = line.split("/")[-1].replace('"', '').strip("\n")
	## TODO: See if Windows' Pictures file changes "location" based on locale
	## macOS 10.14 does not change the actual folder name within the OS if locale is not set to English
	## e.g. - within the OS, the folder will be named "/Users/<username>/Pictures" despite being named "ピクチャー" in Finder
	else:
		picturesFolder = "Pictures"
	## Name the folder based on the tags (or lack thereof)
	if tag == '':
		downloadpath = os.path.join(homeFolder, picturesFolder, "Wallhaven-Random")
	elif ',' in tag: 
		## If there are multiple tags (e.g. anime,girls), have 'tags' turn into "Anime Girls" 
		tags = tag.replace(',', ' ')
		## for the URL, the tags will be placed as "anime+girls"
		tag = tag.replace(',', '+')
		downloadpath = os.path.join(homeFolder, picturesFolder, "Wallhaven Tags - {}".format(tags.title()))
	else:
		downloadpath = os.path.join(homeFolder, picturesFolder, "Wallhaven Tag - {}".format(tag.capitalize()))
	print("Downloading to {}".format(downloadpath))
	## If the folder of "Wallhaven Tag(s) - Tags Here" does not exist, create it. Otherwise, leave it alone.
	if not os.path.isdir(downloadpath):
		os.mkdir(downloadpath)
	## If someone fat-fingers "-p 5" as "-p r" (or is a smartass) this will catch it and set the pages to "10" 
	try:
		pages = int(args.page)
	
	except TypeError:
		pages = 10
		print("Invalid page number entered, setting default of {}.".format(pages))
	
	print("Downloading {} pages, or about {} images".format(pages, pages*24))
	## For the pages 1 - requested amount get all of the images from the page and download them
	for page in range(1, pages+1):
		URL = "https://wallhaven.cc/search?q={}&categories=111&purity={}&atleast={}&ratios={}&sorting=random&order=desc&page={}".format(tag, purity, size, ratio, page)
		if purity == "001":
			rawPage = requests.get(URL, cookies=cookie)
		else:
			rawPage = requests.get(URL)
		parsedPage = bsoup(rawPage.content, 'html.parser')
		imageDump = [a['href'] for a in parsedPage.find_all(name="a", class_="preview")]

		for image in imageDump:
			downloadWallpapers(downloadpath, image)

	finished(downloadpath)

## Start the program!
investigateWallpapers(args.ratio, args.size, args.tag, purity)
